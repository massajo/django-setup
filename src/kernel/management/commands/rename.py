import os
from django.core.management import BaseCommand

class Command(BaseCommand):

    help = "Rename a Django project"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def add_arguments(self, parser):
        parser.add_argument('old_project_name', type=str, help='The old Django project to rename')
        parser.add_argument('new_project_name', type=str, help='The new Django project name')
    
    def handle(self, *args, **options):
        
        old_project_name = options['old_project_name']
        new_project_name = options['new_project_name']

        files_to_rename = [
            old_project_name + '/wsgi.py', 
            old_project_name + '/conf/base.py',
            'manage.py'
        ]
        folder_to_rename = old_project_name

        for _file in files_to_rename:
            with open(_file, 'r') as rf:
                data = rf.read()
            
            data = data.replace(old_project_name, new_project_name)

            with open(_file, 'w') as wf:
                wf.write(data)
        
        os.rename(folder_to_rename, new_project_name)
        self.stdout.write(self.style.SUCCESS('Successfully rename the Django project to "%s" ' % new_project_name))


